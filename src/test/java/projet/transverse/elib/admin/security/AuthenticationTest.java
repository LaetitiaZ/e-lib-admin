package projet.transverse.elib.admin.security;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import projet.transverse.elib.admin.TestApplication;
import projet.transverse.elib.admin.domain.user.User;
import projet.transverse.elib.admin.domain.user.UserRepository;

import java.nio.charset.StandardCharsets;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TestApplication.class)
@Transactional
@AutoConfigureMockMvc
public class AuthenticationTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationProvider provider;

    @Autowired
    MockMvc mockMvc;

    final String VALID_LOGIN = "admin@elib.com";
    final String VALID_PASSWORD = "admin_password";

    @Test
    void should_login_calling_post_login_http_basic_bis() throws Exception {
        String usernamePassword = VALID_LOGIN + ":" + VALID_PASSWORD;
        String encodedUser = Base64Utils.encodeToString(usernamePassword.getBytes(StandardCharsets.UTF_8));
        mockMvc
                .perform(MockMvcRequestBuilders.post("/home").header(HttpHeaders.AUTHORIZATION, "Basic " + encodedUser))
                .andExpect(status().isOk());
    }

}
