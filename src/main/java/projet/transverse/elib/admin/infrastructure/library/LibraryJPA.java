package projet.transverse.elib.admin.infrastructure.library;

import org.springframework.data.jpa.repository.JpaRepository;
import projet.transverse.elib.admin.domain.library.Library;

public interface LibraryJPA extends JpaRepository<Library, String> {

    Library findLibraryByName(String name);
}
