package projet.transverse.elib.admin.infrastructure.library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import projet.transverse.elib.admin.domain.library.Library;
import projet.transverse.elib.admin.domain.library.LibraryRepository;

import java.util.List;
import java.util.Optional;

@Repository
public class LibraryRepositoryImpl implements LibraryRepository {

    @Autowired
    private LibraryJPA jpa;

    @Override
    public void add(Library lib) {
        jpa.save(lib);
    }

    @Override
    public void delete(Library lib) {
        jpa.delete(lib);
    }

    @Override
    public Optional<Library> findById(String id) {
        return jpa.findById(id);
    }

    @Override
    public void deleteById(String id) {
        jpa.deleteById(id);
    }

    @Override
    public List<Library> findAll() {
        return jpa.findAll();
    }

    @Override
    public void update(Library lib) {
        jpa.save(lib);
    }

    @Override
    public Library findByName(String name) {
        return jpa.findLibraryByName(name);
    }
}
