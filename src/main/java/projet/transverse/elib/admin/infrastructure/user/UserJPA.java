package projet.transverse.elib.admin.infrastructure.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import projet.transverse.elib.admin.domain.user.User;

import java.util.Optional;

@Repository
public interface UserJPA extends JpaRepository<User, String> {

    Optional<User> findUserByAccountInfo_Mail(String mail);

}
