package projet.transverse.elib.admin.infrastructure.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import projet.transverse.elib.admin.domain.user.User;
import projet.transverse.elib.admin.domain.user.UserRepository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private UserJPA userJPA;

    @Autowired
    public UserRepositoryImpl(UserJPA userJPA) {
        this.userJPA = userJPA;
    }

    public void add(User user) {
        userJPA.save(user);
    }

    public void update(User user) {
        userJPA.save(user);
    }

    public void delete(User user) {
        userJPA.delete(user);
    }

    @Override
    public Optional<User> findById(String id) {
        return userJPA.findById(id);
    }

    @Override
    public List<User> findAll() {
        return userJPA.findAll();
    }

    @Override
    public Optional<User> findByMail(String mail) {
        return userJPA.findUserByAccountInfo_Mail(mail);
    }

}
