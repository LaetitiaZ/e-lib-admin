package projet.transverse.elib.admin.exposition;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import projet.transverse.elib.admin.application.library.LibraryService;
import projet.transverse.elib.admin.domain.library.Library;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@RestController
@RequestMapping("/library")
public class LibraryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LibraryController.class);

    @Autowired
    LibraryService service;

    @PostMapping("/create")
    void createLibrary(@RequestBody Library library) {
        try {
            service.add(library);
            deployImageToAzure(library);
        }catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    public void deployImageToAzure(Library library) {
        RestTemplate restTemplate = new RestTemplate();

        String pipelineBuildURL = "https://dev.azure.com/e-lib/elib/_apis/build/builds?api-version=6.0";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("definition", new JSONObject().put("id", 1));
        JSONObject parameters = new JSONObject();
        parameters.put("adminMail", library.getAdminMail());
        parameters.put("adminPassword", Base64.getEncoder().encodeToString(library.getAdminPassword().getBytes(StandardCharsets.UTF_8)));
        parameters.put("artifactId", library.getName());
        parameters.put("domainName", library.getName());
        parameters.put("libraryName", library.getName());

        jsonObject.put("templateParameters", parameters);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBasicAuth("elib-trigger","tjeknvls7cpp5tajtxbqqut7ntizltzqn7ucsnmktwhxk2dvp2eq");
        String body = JSONObject.valueToString(jsonObject);
        HttpEntity<String> request = new HttpEntity<>(body, headers);

        String buildMessage = restTemplate.postForEntity(pipelineBuildURL, request, String.class).getBody();
        LOGGER.info("Pipeline server returned: \n"+buildMessage);
    }


}
