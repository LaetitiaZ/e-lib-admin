package projet.transverse.elib.admin.exposition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import projet.transverse.elib.admin.application.library.LibraryService;
import projet.transverse.elib.admin.domain.library.Library;

import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    LibraryService libraryService;

    @GetMapping("/libraries")
    public List<Library> getLibraries() {
        return libraryService.getAll();
    }

}
