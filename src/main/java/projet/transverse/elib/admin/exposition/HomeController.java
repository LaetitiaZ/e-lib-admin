package projet.transverse.elib.admin.exposition;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import projet.transverse.elib.admin.application.user.UserService;
import projet.transverse.elib.admin.domain.user.User;
import projet.transverse.elib.admin.security.CustomAuthProvider;

@RestController
@CrossOrigin("http://localhost:4200")
public class HomeController {

    private static Logger LOGGER = LoggerFactory.getLogger(HomeController.class);
    @Autowired
    UserService userService;

    @Autowired
    AuthenticationProvider provider;

    @GetMapping("/home")
    String getHome() {
        return "home";
    }

    @PostMapping("/home")
    String getHomeFromLogin() {
        return "Hello you came from login";
    }

    @PostMapping("/signup")
    void signup(@RequestBody User user) {
        if(!userService.getOptionalUser(user.getAccountInfo().getMail()).isPresent()) {
            userService.createAccount(user);
        }
    }

}

