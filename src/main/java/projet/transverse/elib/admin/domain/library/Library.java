package projet.transverse.elib.admin.domain.library;


import javax.persistence.*;

@Entity
@Table(name = "library", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
public class Library {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;
    @Column(name = "admin_mail")
    private String adminMail;
    @Column(name = "admin_password")
    private String adminPassword;

    public Library(String name, String adminMail, String adminPassword) {
        this.name = name;
        this.adminMail = adminMail;
        this.adminPassword = adminPassword;
    }

    public Library() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdminMail() {
        return adminMail;
    }

    public void setAdminMail(String adminMail) {
        this.adminMail = adminMail;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public String getName() {
        return name;
    }
}
