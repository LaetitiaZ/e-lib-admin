package projet.transverse.elib.admin.domain.library;


import java.util.List;
import java.util.Optional;

public interface LibraryRepository {

    void add(Library lib);

    void delete(Library lib);

    Optional<Library> findById(String id);

    void deleteById(String id);

    List<Library> findAll();

    void update(Library lib);

    Library findByName(String name);

}
