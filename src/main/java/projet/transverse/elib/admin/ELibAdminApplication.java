package projet.transverse.elib.admin;

import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.sql.SQLException;

@SpringBootApplication
public class ELibAdminApplication extends SpringBootServletInitializer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ELibAdminApplication.class);

    public static void main(String[] args) {
        startH2Server();
        SpringApplication.run(ELibAdminApplication.class);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        startH2Server();
        return application.sources(ELibAdminApplication.class);
    }


    private static void startH2Server() {
        try {
            Server h2Server = Server.createTcpServer("-tcp", "-ifNotExists", "-tcpAllowOthers", "-tcpPort", "9092").start();

            if (h2Server.isRunning(true)) {
                LOGGER.info("H2 server was started and is running.");

                LOGGER.warn("URL "+ h2Server.getURL());
                LOGGER.warn("PORT "+ h2Server.getPort());
            } else {
                throw new RuntimeException("Could not start H2 server.");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to start H2 server: ", e);
        }
    }

}
