package projet.transverse.elib.admin.application.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import projet.transverse.elib.admin.domain.user.User;
import projet.transverse.elib.admin.domain.user.UserRepository;

import java.util.Optional;

@Service
@Transactional
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void createAccount(User user) {
        userRepository.add(user);
    }

    public User getUser(String mail) {
        return userRepository.findByMail(mail)
                .orElseThrow(() -> new IllegalArgumentException("No user found for mail " + mail));
    }

    public Optional<User> getOptionalUser(String mail) {
        return userRepository.findByMail(mail);
    }

}
