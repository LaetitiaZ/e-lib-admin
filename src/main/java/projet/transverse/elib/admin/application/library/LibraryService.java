package projet.transverse.elib.admin.application.library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import projet.transverse.elib.admin.domain.library.Library;
import projet.transverse.elib.admin.domain.library.LibraryRepository;

import java.util.List;

@Service
@Transactional
public class LibraryService {

    private LibraryRepository libraryRepository;

    @Autowired
    public LibraryService(LibraryRepository libraryRepository) {
        this.libraryRepository = libraryRepository;
    }

    public List<Library> getAll() {
        return libraryRepository.findAll();
    }

    public void add(Library library) {
        try {
            libraryRepository.add(library);
        }catch (Exception e) {
            throw new IllegalArgumentException("Library name already exists");
        }

    }




}
